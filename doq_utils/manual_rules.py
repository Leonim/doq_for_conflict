from spacy.matcher import Matcher
import numpy as np

from doq_utils import numutils, config



def create_size_comment_matcher():
    '''
    Matcher that filters out comments regarding the size of the event. Some event notes contain a comment [size= ...] at the end
    '''

    matcher = Matcher(config.nlp.vocab)
    pattern = [
        [{"ORTH": "["}, {"LOWER": "size"}, {"ORTH": "="}, {"IS_DIGIT": True}, {"ORTH": "]"}],
        [{"ORTH": "["}, {"LOWER": "size"}, {"ORTH": "="}, {"IS_ALPHA": True}, {"IS_ALPHA": True}, {"ORTH": "]"}],
        [{"ORTH": "["}, {"LOWER": "size"}, {"ORTH": "="}, {"IS_ALPHA": True}, {"IS_DIGIT": True}, {"ORTH": "]"}],
        [{"ORTH": "["}, {"LOWER": "size"}, {"ORTH": "="}, {"IS_ALPHA": True}, {"ORTH": "]"}],
        [{"LOWER": "fatality"}, {"IS_DIGIT": True}],
        [{"LOWER": "fatalities"}, {"IS_DIGIT": True}],
        [{"LOWER": "fatality"}, {"IS_ALPHA": True}],
        [{"LOWER": "fatalities"}, {"IS_ALPHA": True}],
        [{"IS_DIGIT": True}, {"LOWER": "fatality"}],
        [{"IS_DIGIT":True}, {"LOWER": "fatalities"}],
        [{"IS_ALPHA": True}, {"LOWER": "fatalities"}],
        [{"IS_ALPHA":True}, {"LOWER": "fatality"}],
    ]

    matcher.add("size_comment", pattern)

    return matcher

def create_date_matcher():
    '''returns pattern based mathcher for dates that have an extra space before or after the comma, like: January 13 , 2010.'''
    
    matcher = Matcher(config.nlp.vocab)

    # Write a pattern for those dates --> read the spaCy docs.
    #pattern = [[{"TEXT": "November"}, {"IS_SPACE":True}, {"IS_DIGIT": True} , {"IS_SPACE":True},{"IS_PUNCT":True}, {"IS_SPACE":True} ]]
    pattern = [
        [{"LOWER": "january"},{"IS_DIGIT": True}, {"IS_PUNCT": True}, {"SHAPE":"dddd"}], 
        [{"LOWER": "february"},{"IS_DIGIT": True}, {"IS_PUNCT": True}, {"SHAPE":"dddd"}],
        [{"LOWER": "march"},{"IS_DIGIT": True}, {"IS_PUNCT": True}, {"SHAPE":"dddd"}],
        [{"LOWER": "april"},{"IS_DIGIT": True}, {"IS_PUNCT": True}, {"SHAPE":"dddd"}],
        [{"LOWER": "may"},{"IS_DIGIT": True}, {"IS_PUNCT": True}, {"SHAPE":"dddd"}],
        [{"LOWER": "june"},{"IS_DIGIT": True}, {"IS_PUNCT": True}, {"SHAPE":"dddd"}],
        [{"LOWER": "july"},{"IS_DIGIT": True}, {"IS_PUNCT": True}, {"SHAPE":"dddd"}],
        [{"LOWER": "august"},{"IS_DIGIT": True}, {"IS_PUNCT": True}, {"SHAPE":"dddd"}],
        [{"LOWER": "september"},{"IS_DIGIT": True}, {"IS_PUNCT": True}, {"SHAPE":"dddd"}],
        [{"LOWER": "october"},{"IS_DIGIT": True}, {"IS_PUNCT": True}, {"SHAPE":"dddd"}],
        [{"LOWER": "november"},{"IS_DIGIT": True}, {"IS_PUNCT": True}, {"SHAPE":"dddd"}], 
        [{"LOWER": "december"},{"IS_DIGIT": True}, {"IS_PUNCT": True}, {"SHAPE":"dddd"}],

        [{"LOWER": "january"},{"IS_DIGIT": True}, {"IS_PUNCT": True}], 
        [{"LOWER": "february"},{"IS_DIGIT": True}, {"IS_PUNCT": True}],
        [{"LOWER": "march"},{"IS_DIGIT": True}, {"IS_PUNCT": True}],
        [{"LOWER": "april"},{"IS_DIGIT": True}, {"IS_PUNCT": True}],
        [{"LOWER": "may"},{"IS_DIGIT": True}, {"IS_PUNCT": True}],
        [{"LOWER": "june"},{"IS_DIGIT": True}, {"IS_PUNCT": True}],
        [{"LOWER": "july"},{"IS_DIGIT": True}, {"IS_PUNCT": True}],
        [{"LOWER": "august"},{"IS_DIGIT": True}, {"IS_PUNCT": True}],
        [{"LOWER": "september"},{"IS_DIGIT": True}, {"IS_PUNCT": True}],
        [{"LOWER": "october"},{"IS_DIGIT": True}, {"IS_PUNCT": True}],
        [{"LOWER": "november"},{"IS_DIGIT": True}, {"IS_PUNCT": True}], 
        [{"LOWER": "december"},{"IS_DIGIT": True}, {"IS_PUNCT": True}]]
    # Add the pattern to the matcher and apply the matcher to the doc
    
    matcher.add("DATE", pattern)
    
    return matcher

def extract_dobj_quant_triples(doc, date_indices):
    triples = []
    for token in doc:
        if (date_indices[token.i] == 1): continue # we are not interested in dates
        triple = []
        if (token.pos_ == "NUM"):
            if (token.head.dep_ == "dobj"):
                token_dobj = token.head
               # if (token_dobj.head.pos_ == "VERB"):
                if (token_dobj.head.dep_ == "ROOT"):
                    num = numutils.number_to_int(token)
                    if (num != None):
                        triple.append((token_dobj.head.lemma_, num,token_dobj.lemma_))
                        triples.append(triple)
        elif (token.dep_ == "det"):
            if (token.head.dep_ == "dobj"):
                token_dobj = token.head
              #  if (token_dobj.head.pos_ == "VERB"):
                if (token_dobj.head.pos_ == "ROOT"):
                    triple.append((token_dobj.head.lemma_, 1,token_dobj.lemma_))
                    triples.append(triple)

    return triples

def extract_nsubjpass_quant_triples(doc, date_indices):
    triples = []
    for token in doc:
        if (token.pos_ == "NUM"):
            if (date_indices[token.i] == 1): continue
            triple = []
            if (token.head.dep_ == "nsubjpass" and token.head.head.pos_ == "VERB"):
           # if (token.head.dep_ == "nsubjpass" and token.head.head.pos_ == "ROOT"):
                num = numutils.number_to_int(token)
                if (num != None):
                    triple.append((token.head.head.lemma_, num, token.head.lemma_))
                    triples.append(triple)
        elif (token.dep_ == "det"):
            triple = []
            if (token.head.dep_ == "nsubjpass" and token.head.head.pos_ == "VERB"):
       #     if (token.head.dep_ == "nsubjpass" and token.head.head.pos_ == "ROOT"):
                triple.append((token.head.head.lemma_, 1, token.head.lemma_))
                triples.append(triple)
    
    return triples

def extract_verb_count_tuples(doc, window_size, date_indices):
    '''Extracts tuples (verb, nunber) within a given window size of the verb. '''
    tuples = []
    for token in doc:
        if (token.pos_ == 'VERB'):
            num = 0
            found = False
            for context in doc[token.i-window_size:token.i+window_size]:
                if (found == False):
                    if (context.pos_ == "NUM" and date_indices[context.i]==0):
                        num = numutils.number_to_int(context)
                        if (num != None): 
                            tuples.append(tuple((token.lemma_, num)))
                            found = True # Found a number count in context
            if (found == False): # Did not find a number count --> simply count as 1
                tuples.append(tuple((token.lemma_, 1)))
            
    return tuples

def extract_noun_count_tuples(doc, window_size, date_indices):
    '''Extracts tuples (noun, nunber) within a given window size of the verb. '''
    tuples = []
    for token in doc:
        if (token.pos_ == 'NOUN'):
            num = 0
            found = False
            for context in doc[np.max((0,token.i-window_size)): np.min((token.i+window_size, len(doc)))]:
                if (found == False):
                    if (context.pos_ == "NUM" and date_indices[context.i]==0):
                        num = numutils.number_to_int(context)
                        if (num != None): 
                            tuples.append(tuple((token.lemma_, num)))
                            found = True # Found a number count in context
            if (found == False): # Did not find a number count --> simply count as 1
                tuples.append(tuple((token.lemma_, 1)))
            
    return tuples

def extract_main_action_tuples(doc, date_indices):
    tuples = []
    for token in doc:
        if (token.dep_ == "nsubj"):
            if (token.head.pos_ == "VERB"):
                print(token.head.text)
                num = 1
                for child in token.head.ancestors:
                    print(child.text)
                    if (numutils.token_is_number_but_not_date(child, date_indices)):
                        num = numutils.number_to_int(child)
                        break
                tuples.append((token.head.lemma_, num))
    print(tuples)
    return tuples


def main_action_is_interesting(note, action_set):
    doc = config.nlp(note)
    for token in doc:
        if (token.dep_ == "nsubj" or token.dep_ == "nsubjpass"):
            if (token.head.lemma_ in action_set):
                return True
    return False
            
                    
                    






