from doq_utils import config, preprocessing_nn
import torchtext.legacy.data as data
import torchtext
import pickle
from sklearn.model_selection import train_test_split
import torch
from transformers import BertTokenizer
import logging

def extend_data_parse(data_doq):
    # Data frame for dependency parse
    
    data_doq_dep = []
    data_doq_ent = []
    data_doq_pos = []

    for index, row in data_doq.iterrows():
        doc = config.nlp(row['text'])
        deps = []
        ents = []
        pos = []
        for token in doc:
            deps.append(token.dep_)
            ents.append(token.ent_type_)
            pos.append(token.pos_)
        data_doq_dep.append(deps)
        data_doq_ent.append(ents)
        data_doq_pos.append(pos)

    data_doq['dep'] = data_doq_dep
    data_doq['ent'] = data_doq_ent
    data_doq['pos'] = data_doq_pos


def tokenize_and_cut(sentence):
    '''
    Tokenizer function used in case bert embedding is used
    '''

    max_input_length = config.bert_tokenizer.max_model_input_sizes['bert-base-uncased']
    tokens = config.bert_tokenizer.tokenize(sentence) 
    tokens = tokens[:max_input_length-2]
    return tokens

def create_fields(embedding):
    '''
    Create the torchtext fields needed for the models. TEXT field is created differently depending on which embedding is used 
    '''
    if (embedding == 'bert'):

        tokenizer = config.bert_tokenizer
        init_token_idx = tokenizer.cls_token_id
        eos_token_idx = tokenizer.sep_token_id
        pad_token_idx = tokenizer.pad_token_id
        unk_token_idx = tokenizer.unk_token_id

        TEXT = data.Field(batch_first = True,
                    use_vocab = False,
                    tokenize = tokenize_and_cut,
                    preprocessing = tokenizer.convert_tokens_to_ids,
                    init_token = init_token_idx,
                    eos_token = eos_token_idx,
                    pad_token = pad_token_idx,
                    unk_token = unk_token_idx)
    else:
        tokenizer = None # we don't define our own tokenizer if we don't use Bert
        TEXT = data.Field(tokenize='spacy', sequential=True, lower=True, include_lengths=True)

    LABEL = data.LabelField(dtype =float)
    DEP = data.Field()
    POS = data.Field()
    ENT = data.Field()

    fields = [('text',TEXT), ('label',LABEL), ('dep', DEP), ('ent', ENT), ('pos', POS)]

    return TEXT, LABEL, DEP, POS, ENT, fields, tokenizer


class DataFrameDataset(data.Dataset):

    def __init__(self, df, fields, is_test=False, **kwargs):

        examples = []

        for i, row in df.iterrows():
            #label = row.target if not is_test else None  # changed this # TODO: decide whether this makes sense or not. should I pass the true label to the test set? 
            label = row.target 
            text = row.text
            dep = row.dep
            ent = row.ent
            pos = row.pos
            examples.append(data.Example.fromlist([text, label, dep, ent, pos], fields))
        

        super().__init__(examples, fields, **kwargs)

    @staticmethod
    def sort_key(ex):
        return len(ex.text)

    @classmethod
    def splits(cls, fields, train_df, val_df=None, test_df=None, **kwargs):
        train_data, val_data, test_data = (None, None, None)
        data_field = fields

        if train_df is not None:
            train_data = cls(train_df.copy(), data_field, **kwargs)
        if val_df is not None:
            val_data = cls(val_df.copy(), data_field, **kwargs)
        if test_df is not None:
            test_data = cls(test_df.copy(), data_field, True, **kwargs)

        return tuple(d for d in (train_data, val_data, test_data) if d is not None)

def create_train_val_test_split(data_doq, fields, val_size=0.3, test_size=0.2):
    '''
    Split preprocessed data in train, val and test set and feed to predefined fields

    
    '''
    
    train_df, test_df = train_test_split(data_doq, test_size=0.2)
    train_df, valid_df = train_test_split(train_df, test_size = 0.3)
    train_ds, val_ds, test_ds = DataFrameDataset.splits(fields, train_df=train_df, val_df=valid_df, test_df=test_df) 
    
    logging.info(f"Number of training examples: {len(train_df)}")
    logging.info(f"Number of validation examples: {len(valid_df)}")
    logging.info(f"Number of testing examples: {len(test_df)}")

    return train_ds, val_ds, test_ds


def build_vocabs(TEXT, LABEL, DEP, POS, ENT, embedding, train_ds, task, relevant_fatality_numbers,  create_vocab=False):
    '''
    Build the vocabularies for all the different fields, based on the training data (not val and test data).
    Whether a vocabulary for the text is created depends on whether we use a bert embedding.
    '''
    vocab_path = 'vocab/'+embedding+'_vocab_' + task + '.pkl'
    if (embedding != 'bert'):
        # We only need to build a vocabulary if we don't use the Bert embedding. In case of Bert, the text vocabulary is already fixed
        if (create_vocab == True): 

            MAX_VOCAB_SIZE = 25000
            TEXT.build_vocab(train_ds, 
                            max_size = MAX_VOCAB_SIZE, 
                            vectors = torchtext.vocab.Vectors('glove.840B.300d.txt'),
                            unk_init = torch.Tensor.zero_)


            preprocessing_nn.save_vocab(TEXT.vocab, vocab_path)


        with open(vocab_path, 'rb') as f:
            TEXT.vocab = pickle.load(f)

    LABEL.build_vocab(train_ds)
    LABEL.vocab.stoi = {i:i for i in relevant_fatality_numbers} # cheap solution to creating the dictionary for out of vocabulary fatality numbers
    logging.info('label vocabulary', LABEL.vocab.stoi)
    DEP.build_vocab(train_ds)
    POS.build_vocab(train_ds)
    ENT.build_vocab(train_ds)


def save_vocab(vocab, path):
    output = open(path, 'wb')
    pickle.dump(vocab, output)
    output.close()