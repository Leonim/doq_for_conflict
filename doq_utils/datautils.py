from nltk.corpus import stopwords
import numpy as np
import random
import pandas as pd
import spacy
from pathlib import Path



# Import homegrown functions
from doq_utils import utils, manual_rules, numutils, config

''' Functions'''

def xlsx_to_csv(filename_xlsx, sheet_xlsx, filename_csv):
    """
    Converts xlsx file to csv file.

        Args:
        filename_xlsx: filename (including path) of the Excel file
        sheet_xlsx: Name of sheet in the Excel file to convert
        filename_csv: filename (including path) of the new csv file

    """ 
    file = Path(filename_csv)

    if file.exists():
        print('Csv file exists already, check whether it contains the desired data.')
    else:
        data_xls = pd.read_excel(filename_xlsx, sheet_xlsx, index_col=None)
        data_xls.to_csv(filename_csv, encoding='utf-8', index=False)



def load_and_preprocess_ACLED(n_rows, remove_size_count, context, relevant_fatality_numbers, balance_dataset_evenly):
    '''
    Load ACLED data into datafame, add columns if needed and possibly shuffle. Can also load only part of the dataset

    '''

    # Load data as dataframe,  last argument 'TRUE' to shuffle the rows
    data_doq = acled_data_to_df('data/gender_Feb26.xlsx', 'Sheet1', 'data/gender.csv', True) 

    if (n_rows!= -1):
        if (n_rows >= len(data_doq)):
            print("N_rows is not smaller than the length of the dataframe, which is", len(data_doq))
        else:
            # Cut size of dataframe if speicified
            data_doq = data_doq[:n_rows]

    # Additional columns to store extracted quantities 
    additional_columns = [('QUANTITY', 0), ('SIZE_ANNOTATED', -1)]
    data_doq = extend_dataframe_by_columns(data_doq, additional_columns)
    
    # Remove size count if asked
    if (remove_size_count == True): 
        for idx, row in data_doq.iterrows():
            find_annotated_size_acled(row)
    
    # Choose columns
    cols = ['NOTES', 'FATALITIES'] + context
    data_doq = data_doq[cols]

    # Deal with numerical values
    data_doq = prepare_numeric_cols(data_doq, ['FATALITIES'])

    



    
    
    data_doq = data_doq.rename(columns = {'NOTES': 'text', 'FATALITIES' : 'target'})

    # Clean text
    data_doq.loc[:,'text'] = utils.normalise_text(data_doq['text'])

    # If specified, only choose notes that have a certain number of fatalities, i.e. [1,2]
    if (relevant_fatality_numbers != []):

        if (balance_dataset_evenly==True):
            data_doq = create_balanced_data_doq(relevant_fatality_numbers, data_doq, 'target')
        else:
            data_doq = data_doq[data_doq['target'].isin(relevant_fatality_numbers)]

    return data_doq


def load_and_preprocess_EMM(n_rows, context, target, relevant_fatality_numbers, balance_dataset_evenly):
    '''
    Load EEM data into datafame, add columns if needed and possibly shuffle. Can also load only part of the dataset
    n_rows: Number of events to load
    context: additional columns to load next to the event description, like 'Type' or 'Event date'
    target: target number, choose between dead and injured

    '''

    # Load data as dataframe,  last argument 'TRUE' to shuffle the rows
    data_emm = csv_data_to_df('data/emm_full.csv',True) 

    if (n_rows!= -1):
        if (n_rows >= len(data_emm)):
            print("N_rows is not smaller than the length of the dataframe, which is", len(data_emm))
        else:
            # Cut size of dataframe if specified
            data_emm = data_emm[:n_rows]
    
    # Only keep relevant columns
    cols = ['Snippet', target] + context
    data_emm = data_emm[cols]

    # Preprocess numeric columns
    data_emm = prepare_numeric_cols(data_emm, [target])
    data_emm = data_emm.rename(columns = {'Snippet': 'text', target : 'target'})


    # Clean text
    data_emm.loc[:,'text'] = utils.normalise_text(data_emm['text'])

    # If specified, only choose notes that have a certain number of fatalities, i.e. [1,2]
    if (relevant_fatality_numbers != []):

        if (balance_dataset_evenly==True):
            data_emm = create_balanced_data_doq(relevant_fatality_numbers, data_emm, 'target')
        else:
            data_emm = data_emm[data_emm['target'].isin(relevant_fatality_numbers)]
    
    return data_emm


def csv_data_to_df(filepath, shuffle_rows = True):
    ''''
    Load csv data file and return as pandas dataframe, with shuffled rows if specified (necessary to shuffle dates, too)
    '''
    data = pd.read_csv(filepath)
    if (shuffle_rows):
        data = pd.concat([data[:1], data[1:].sample(frac=1)]).reset_index(drop=True)
    
    return data


def acled_data_to_df(source_path, sheet_name, target_path, shuffle_rows = True):
    '''
    Convert Acled xlsx data to csv file and then the csv file to dataframe
    args:
        source_path: string with path to xlsx acled data file
        sheet_name: Name of the excel sheet that contains the infomration to be retrieved. Default set to "Sheet1"
        target_path: string with path to where the csv file should be stored
        shuffle_rows: Option to shuffle the rows so that when I try sth. on the first few entries I get representative results
        
    '''
    xlsx_to_csv(source_path, sheet_name, target_path)
    # These colnames are specific to the ACLED data
    colnames = ['ISO','EVENT_ID_CNTY','EVENT_ID_NO_CNTY','EVENT_DATE','YEAR','TIME_PRECISION','EVENT_TYPE','SUB_EVENT_TYPE','ACTOR1','ASSOC_ACTOR_1','INTER1','ACTOR2','ASSOC_ACTOR_2','INTER2','INTERACTION','REGION','COUNTRY','ADMIN1','ADMIN2','ADMIN3','LOCATION','LATITUDE','LONGITUDE','GEO_PRECISION','SOURCE','SOURCE_SCALE','NOTES','FATALITIES','TIMESTAMP']
    data = pd.read_csv(target_path, names=colnames)
    
    if (shuffle_rows):
        data = pd.concat([data[:1], data[1:].sample(frac=1)]).reset_index(drop=True)

    return data

def prepare_numeric_cols(data, numeric_cols, drop_zero = True, make_bins = True):
    
    for colname in numeric_cols:
        data.loc[:, colname] = pd.to_numeric(data[colname], errors = 'coerce')
        data.loc[:,colname] = data[colname].fillna(0) # Take
        data.loc[:,colname] = data[colname].astype('int')
    
    # drop all events with zero people affected
    if (drop_zero == True):
        data = data[data[colname]!= 0]
    
    if (make_bins == True):
        # annotated numbers to binary bins. 
        data.loc[:,colname] = data.apply(lambda x: utils.my_log2(x[colname]), axis=1)
    
    return data



def update_notes_if_size_annotated(doc, matches):
    '''If the size is annotated in the note, the annotation will be cut out - parsing the annotation adds information that we want to extract from the text.

    doc: parsed note
    matches: array that is either empty (if no specific size/fatality annotation detected) or contains the start and end indices of the annotation
    
    '''
    for match_id, start, end in matches:
        doc = doc[:start] # Since the annotation is at the end of the note, we can simply cut it 
    return doc.text

def find_annotated_size_acled(row):
    '''
    For a given row in the ACLED data, check whether the note contains an annotation on the size of the event. If so, cut the note so that it does not contain the comment anymore and save the size information in an extra column (SIZE_ANNOTATION). If no size annotation, SIZE_ANNOTATED remains -1.

    Args:
        row: Row of a dataframe that contains the ACLED data
    '''
    # Look for patterns that match the size annotation
    size_matcher = manual_rules.create_size_comment_matcher() 
    doc = config.nlp(row["NOTES"])
    matches = size_matcher(doc)
    
    # If match found, update the note so that it does not contain the comment anymore
    row.loc["NOTES"] = update_notes_if_size_annotated(doc, matches)

    # If size comment found, store the quantity in "SIZE_ANNOTATED" 
    size = numutils.extract_quantity_from_annotation(doc, matches)
    row.loc["SIZE_ANNOTATED"] = size



def extend_dataframe_by_columns(df, column_list):

    for column in column_list:
        name = column[0]
        default_value = column[1]
        df[name] = default_value
    
    return df




def create_balanced_data_doq(value_list, data_doq, sample_item):
    
    data_doq = data_doq[data_doq[sample_item].isin(value_list)] # Drop all rows with differen targets than the ones in the list
    value_counts = data_doq[sample_item].value_counts() # count how many times each target value appears
    min_count = np.min([value_counts[x] for x in value_list]) # which target appears the minimal number of times?
    data_doq_sample = data_doq.groupby(sample_item).sample(n = min_count, random_state = 1) # sample same number of rows for each target
    print(f'Now all samples appear {min_count} times.')
    return data_doq_sample
