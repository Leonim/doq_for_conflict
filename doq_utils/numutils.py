from string import punctuation
from os import listdir
from collections import Counter
from nltk.corpus import stopwords
import numpy as np
import random
import pandas as pd
import spacy
from spacy.matcher import Matcher
from pathlib import Path

from word2number import w2n
import locale

from doq_utils import utils, config, manual_rules




def num_quantifiers(note, matcher):	
    '''Counts the number of quantifiers in a string, without dates. Not guaranteed to filter out all dates.
    Args:
        note: string
        matcher: matcher that identifies most dates that are not filtered out by named entity recognition
    Returns:
        num_quant: total number of quantifiers identified
    '''
    
    doc = utils.parse_string(note)

    date = np.zeros(len(doc))
    matches = matcher(doc)
    for match_id, start, end in matches: # Iterate over the matches and print the span text
        date[start:end] = 1
    
    num_quant = 0
    for token in doc:
        if (token.like_num and token.ent_type_ != "DATE"  and date[token.i] != 1):
            num_quant = num_quant+1
    
    return num_quant




def ratio_notes_with_quantifier(note_list, matcher):
    '''Computes the ratio of notes that contain at least one quantifier --> to see how relevant numeracy is for that list of notes
    Args:
        note_list: List of strings
        matcher: spacy matcher that has rules to detect
    Returns:
        ratio_quant: Ratio of notes that contain at least quantifier over all notes
        ratio_quant_per_note: Average number of quantifier in notes that do contain quantifier '''
   
    num_notes_with_quant = 0
    total_quant = 0

    # Go over all notes and count the number of quantifiers in each note
    for note in note_list:
        n_quant_in_note = num_quantifiers(note, matcher)
        if (n_quant_in_note > 0):
            num_notes_with_quant = num_notes_with_quant + 1 # Update number of notes that contain at least one quantifier
        total_quant = total_quant + n_quant_in_note # Update total number of quantifiers found
    
    # Calculate the ratios 
    ratio_quant = num_notes_with_quant/len(note_list) # Ratio of notes that contain quantifier over all notes
    if (num_quant_notes > 0):
        ratio_quant_per_note = total_quant/num_quant_notes # Average number of quantifier in notes that do contain quantifier
    else: 
        ratio_quant_per_note = 0.0

    print(f"ratio of quantified notes: {ratio_quant}")
    print(f"mult occurences ratio: {ratio_quant_per_note}")

    return ratio_quant, ratio_quant_per_note

def mark_date_indices_in_doc(doc):
    '''
    For a given parse, creates an array of the same length that marks the indices of tokens belonging to dates as "1" and everyhting else as "0".

    Args:
        doc: parse of a string
    Returns:
        date_indices: 1D Array of the same length as doc, indices of dates in doc marked as 1, else 0
    '''
    date_indices = np.zeros(len(doc))
    matcher = manual_rules.create_date_matcher()
    matches = matcher(doc)
    # Iterate over the matches and print the span text
    for match_id, start, end in matches:
        date_indices[start:end] = 1
    
    for token in doc: 
        if (token.ent_type_ == "DATE"): 
            date_indices[token.i] = 1
    
    return date_indices

def token_is_number_but_not_date(token, date_indices):
    return (token.like_num and date_indices[token.i] == 0)


def number_to_int(token):
    '''A token may contain a number which is not in int format but as a wort: "two", "hundred", or with different notation: "10'000" instead of "10000". This function makes converts those formats to ints. If the format is not recognized, the function is passed
    Args:
        token: token that should describe a number, can be integer, written form ("hundred") or with thousand markers: "10'000" 
    Returns:
        num: The number of the token as an integer. If the number isn't recognized, returns "None"
    '''
    
    if (token.is_alpha): # e.g. "two"
        try:
            num = w2n.word_to_num(token.text)
            return num
        except:
            pass # Format not recognized
    else:
        try:
            num = locale.atoi(token.text) # e.g. "10'000"
            return num
        except: 
            pass # Format not recognized



def quantities_for_object_in_note(note, object):
    '''
    For a given object and a string, probably noun (?), creates a list of all the quantities in which the object occurs
    
    '''
    object_count_in_note = []
    doc = config.nlp(note)
    date_indices = mark_date_indices_in_doc(doc) # Filter out dates so that they are not "counted"
    for token in doc:
        if token_is_number_but_not_date(token, date_indices): # Case 1: a number relates to the object
            # convert token to int
            num = number_to_int(token)
            if (num != None and token.head.lemma_ == object): # Case 2: a determiner relates to the objec
                object_count_in_note.append(num)
        elif (token.dep_ == "det"):
            if (token.head.lemma_ == object):
                object_count_in_note.append(1)

    return object_count_in_note



def collect_quantities_for_object(note_list, object):
    '''Iterate over list of notes and append quantifiers for a given object to a list'''
    object_count = []
    for note in note_list:
        object_count = object_count + quantities_for_object_in_note(note, object)
    return object_count

def count_nsubjpass_for_action(note, action_set, object_set):
    '''Check whether the note describes a specific action and if so, whether our interest group is affected'''
    doc = config.nlp(note)
    date_indices = mark_date_indices_in_doc(doc)
    triples_nsubjpass = manual_rules.extract_nsubjpass_quant_triples(doc, date_indices)
    num = 0
    for triple in triples_nsubjpass:
        if ((triple[0][0].lower() in action_set) and (triple[0][2].lower() in object_set)):
            num = num + triple[0][1]
            
    return num

def count_quant_for_main_action(note, action_set):
    '''
    Collect all quantifiers of the form "subject - action -  quantifier
    '''
    num = 0
    doc = config.nlp(note)
    date_indices = mark_date_indices_in_doc(doc)
    tuples_main_action = manual_rules.extract_main_action_tuples(doc, date_indices)
    for t in tuples_main_action:
        if((t[0].lower() in action_set)):
            num = num + t[1]
    
    return num


def count_dobj_for_action(note, action_set, object_set):
    '''Check whether the note describes a specific action and if so, whether our interest group is affected'''
    doc = config.nlp(note)
    date_indices = mark_date_indices_in_doc(doc)
    triples_dobj = manual_rules.extract_dobj_quant_triples(doc, date_indices)
    num = 0
    for triple in triples_dobj:
        if ((triple[0][0].lower() in action_set) and (triple[0][2].lower() in object_set)):
            num = num + triple[0][1]
            
    return num



def extract_action_count_in_window(note, window_size, action_set):
    window_size = 10
    doc = config.nlp(note)
    date_indices = mark_date_indices_in_doc(doc)
    verb_count_tuple = manual_rules.extract_verb_count_tuples(doc, window_size, date_indices)
    num = 0
    for tuple in verb_count_tuple:
        if (tuple[0] in action_set): 
            num = num + tuple[1]

    return num

def extract_noun_count_in_window(note, window_size, noun_set):
    window_size = 10
    doc = config.nlp(note)
    date_indices = mark_date_indices_in_doc(doc)
    noun_count_tuple = manual_rules.extract_noun_count_tuples(doc, window_size, date_indices)
    num = 0
    for tuple in noun_count_tuple:
        if (tuple[0] in noun_set): 
            num = num + tuple[1]

    return num

def extract_quantity_from_annotation(doc, matches):
    size = -1
    for match_id, start, end in matches:
        if (doc[start:end].text == '[size=no report]'):
            size = -2
        else:
            for token in doc[start:end]:
                if (token.like_num):
                    size = number_to_int(token)
    
    return size
        
