import torch.nn as nn
import torch

from doq_utils import  config


class LSTM_doq(nn.Module):
    def __init__(self, vocab_size, vocab_dep_size, embedding_dim, hidden_dim, output_dim, n_layers, 
                 bidirectional, dropout, pad_idx, pad_idx_dep):
        
        super().__init__()
        
        self.embedding = nn.Embedding(vocab_size, embedding_dim, padding_idx = pad_idx)
        self.embedding_dep = nn.Embedding(vocab_dep_size, embedding_dim, padding_idx = pad_idx_dep)
      #  self.embedding_pos = nn.Embedding(vocab_pos_size, emebdding_pos_dim, padding_idx = padding_pos_idx)
       # self.embedding_ent = nn.Embedding(vocab_ent_size, emebdding_ent_dim, padding_idx = padding_ent_idx)
        self.rnn = nn.LSTM(2*embedding_dim, 
                           hidden_dim, 
                           num_layers=n_layers, 
                           bidirectional=bidirectional, 
                           dropout=dropout)
        
        self.fc1 = nn.Linear(hidden_dim * 2, hidden_dim) 
        self.fc2 = nn.Linear(hidden_dim, output_dim)
        self.dropout = nn.Dropout(dropout)
        
    def forward(self, text, text_lengths, text_dep):
        
        # text = [sent len, batch size]
        embedded_text = self.embedding(text) # glove embedding
        
        embedded_dep = self.embedding(text_dep)
        embedded = torch.cat((embedded_text, embedded_dep), 2)

        # Concatenate or stack
        
        # embedded = [sent len, batch size, emb dim]

        #pack sequence
        packed_embedded = nn.utils.rnn.pack_padded_sequence(embedded, text_lengths)
        packed_output, (hidden, cell) = self.rnn(packed_embedded)
        
        #unpack sequence
        # output, output_lengths = nn.utils.rnn.pad_packed_sequence(packed_output)

        # output = [sent len, batch size, hid dim * num directions]
        # output over padding tokens are zero tensors
        
        # hidden = [num layers * num directions, batch size, hid dim]
        # cell = [num layers * num directions, batch size, hid dim]
        
        # concat the final forward (hidden[-2,:,:]) and backward (hidden[-1,:,:]) hidden layers
        # and apply dropout
        
        hidden = self.dropout(torch.cat((hidden[-2,:,:], hidden[-1,:,:]), dim = 1))
        output = self.fc1(hidden)
        output = self.dropout(self.fc2(output))
                
        #hidden = [batch size, hid dim * num directions]
            
        return output

class LSTM_doq_pure(nn.Module):
    def __init__(self, vocab_size, vocab_dep_size, embedding_dim, hidden_dim, output_dim, n_layers, 
                 bidirectional, dropout, pad_idx, pad_idx_dep):
        
        super().__init__()


        self.embedding = nn.Embedding(vocab_size, embedding_dim, padding_idx = pad_idx)
        self.embedding_dep = nn.Embedding(vocab_dep_size, embedding_dim, padding_idx = pad_idx_dep)
      #  self.embedding_pos = nn.Embedding(vocab_pos_size, emebdding_pos_dim, padding_idx = padding_pos_idx)
       # self.embedding_ent = nn.Embedding(vocab_ent_size, emebdding_ent_dim, padding_idx = padding_ent_idx)
        self.rnn = nn.LSTM(embedding_dim, 
                           hidden_dim, 
                           num_layers=n_layers, 
                           bidirectional=bidirectional, 
                           dropout=dropout)
        
        self.fc1 = nn.Linear(hidden_dim * 2, hidden_dim) 
        self.fc2 = nn.Linear(hidden_dim, output_dim)
        self.dropout = nn.Dropout(dropout)
        
    def forward(self, text, text_lengths, text_dep):
        
        # text = [sent len, batch size]
        

        embedded = self.embedding(text) # glove embedding
       # embedded_dep = self.embedding(text_dep)
       # embedded = torch.cat((embedded_glove, embedded_dep), 2)

        # Concatenate or stack
        
        # embedded = [sent len, batch size, emb dim]

        #pack sequence
        packed_embedded = nn.utils.rnn.pack_padded_sequence(embedded, text_lengths)
        packed_output, (hidden, cell) = self.rnn(packed_embedded)
        
        #unpack sequence
        # output, output_lengths = nn.utils.rnn.pad_packed_sequence(packed_output)

        # output = [sent len, batch size, hid dim * num directions]
        # output over padding tokens are zero tensors
        
        # hidden = [num layers * num directions, batch size, hid dim]
        # cell = [num layers * num directions, batch size, hid dim]
        
        # concat the final forward (hidden[-2,:,:]) and backward (hidden[-1,:,:]) hidden layers
        # and apply dropout
        
        hidden = self.dropout(torch.cat((hidden[-2,:,:], hidden[-1,:,:]), dim = 1))
        output = self.fc1(hidden)
        output = self.dropout(self.fc2(output))
                
        #hidden = [batch size, hid dim * num directions]
            
        return output
    

def freeze_bert_params(model):
    '''
For a model that has Bert embeddings, freeze all the bert parameters
'''
    for name, param in model.named_parameters():
        if name.startswith('bert'):
            param.requires_grad = False


class BERTGRUSentiment(nn.Module): # TODO: check whether I need to define batchfirst when initalizing the field
    def __init__(self, bert,
                 hidden_dim,
                 output_dim,
                 n_layers,
                 bidirectional,
                 dropout):
        
        super().__init__()
        
        self.bert = bert
        embedding_dim = config.bert.config.to_dict()['hidden_size'] # TODO: Implement embedding for dependency parse etc too, can look at Dog pure
        self.rnn = nn.GRU(embedding_dim,
                          hidden_dim,
                          num_layers = n_layers,
                          bidirectional = bidirectional,
                          batch_first = True,
                          dropout = 0 if n_layers < 2 else dropout) # TODO: consider using an LSTM instead of GRU

        
        self.out = nn.Linear(hidden_dim * 2 if bidirectional else hidden_dim, output_dim)
        self.dropout = nn.Dropout(dropout)
        
    def forward(self, text):
        
        #text = [batch size, sent len]        
        with torch.no_grad():
            embedded = self.bert(text)[0]
                
        #embedded = [batch size, sent len, emb dim]
        _, hidden = self.rnn(embedded)
        
        #hidden = [n layers * n directions, batch size, emb dim]
        if self.rnn.bidirectional:
            hidden = self.dropout(torch.cat((hidden[-2,:,:], hidden[-1,:,:]), dim = 1))
        else:
            hidden = self.dropout(hidden[-1,:,:])
                
        #hidden = [batch size, hid dim]
        output = self.out(hidden)
        
        #output = [batch size, out dim]
        return output


def initialize_model(embedding, model_type, TEXT, DEP, OUTPUT_DIM):

    INPUT_DIM_DEP = len(DEP.vocab)
    PAD_IDX_DEP = DEP.vocab.stoi[DEP.pad_token]
    
    HIDDEN_DIM = 256
    
    N_LAYERS = 2
    BIDIRECTIONAL = True
    DROPOUT = 0.2

    if (embedding == 'bert'):

        model = BERTGRUSentiment(config.bert,
                         HIDDEN_DIM,
                         OUTPUT_DIM,
                         N_LAYERS,
                         BIDIRECTIONAL,
                         DROPOUT)
        
        freeze_bert_params(model)
    
    else:
        INPUT_DIM = len(TEXT.vocab)
        EMBEDDING_DIM = 300
        PAD_IDX = TEXT.vocab.stoi[TEXT.pad_token] # padding

        if (model_type=='pure'):
            model = LSTM_doq_pure(INPUT_DIM, INPUT_DIM_DEP,
                EMBEDDING_DIM, 
                HIDDEN_DIM, 
                OUTPUT_DIM, 
                N_LAYERS, 
                BIDIRECTIONAL, 
                DROPOUT, 
                PAD_IDX, PAD_IDX_DEP)
        else:
            model = LSTM_doq(INPUT_DIM, INPUT_DIM_DEP,
                EMBEDDING_DIM, 
                HIDDEN_DIM, 
                OUTPUT_DIM, 
                N_LAYERS, 
                BIDIRECTIONAL, 
                DROPOUT, 
                PAD_IDX, PAD_IDX_DEP)

            model.embedding_dep.weight.data[PAD_IDX_DEP] = torch.zeros(EMBEDDING_DIM)


        pretrained_embeddings = TEXT.vocab.vectors
        model.embedding.weight.data.copy_(pretrained_embeddings)
        model.embedding.weight.data[PAD_IDX] = torch.zeros(EMBEDDING_DIM)
    
    return model
    

    