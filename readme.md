All about the DoQ extraction method in the "doq_extration" folder. I am currently still trying out new functions in the playground.

# To do

- [ ] Look at WordNet for synonyms for "kill" etc
- [ ] Consider object oriented programming for extraction method
- [ ] Look at FrameNet/PropBank --> can they be used for our extraction method?
- [ ] Look at Riedels code: numerate language models
- [ ] Parse notes using petrarch again to see whether there is any additional information about the event types that can be used to enhance the doq dataset
- [ ] define extraction rules manually


Requirements:
['builtins',
 'builtins',
 'torch',
 'pandas',
 'numpy',
 'torch.nn',
 'torch.optim',
 'torch.nn.functional',
 'torchtext',
 'nltk',
 'random',
 'pyprind',
 'doq',
 'spacy',
 'torchtext.legacy.data',
 'types']


 Embeddings need to be downloaded
 