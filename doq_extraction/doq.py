import numpy as np
import spacy

import argparse

from doq_utils import utils, datautils, numutils, manual_rules, datautils, doq_models
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
import sys
import json
import logging
import seaborn as sns

def parse_shell_args(args):

    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--n_rows', help='Specify if you want to cut the size of the dataset', type=int, default=-1)
    parser.add_argument('--skip_irrelevant', help='Skip notes in which the main verb is not in the action set', type=bool, default = True)
    parser.add_argument('--doq_methods', nargs='*', help='Extraction methods to use', required=False)
    parser.add_argument('--nsubj_pass', type=bool, help= 'Extract actions related to passive subject', default = True)
    parser.add_argument('--dobj_verb', type=bool, help= 'Extract actions happening to direct opject', default = True)
    parser.add_argument('--noun_window', type=bool, help= 'Extract numbers in reach of noun', default = True)
    parser.add_argument('--verb_window', type=bool, help= 'Extract numbers in reach of verb', default = True)
    parser.add_argument('--window_size', type = int, help='Size of context window in both directions', default = 5)
    parser.add_argument('--plot_doq', type = bool, help= ' Whether to create a doq plot', default  = False )
    # Add arguments for the action, noun and object set
    # If verb-dobj method used:
    # parser.add_argument('--action_set', nargs='+', help='Verbs relevant for extraction', required=False)
    # parser.add_argument('--noun_set', nargs='+', help='Verbs relevant for extraction', required=False)
    # parser.add_argument('--object_set', nargs='+', help='Verbs relevant for extraction', required=False)
    # Parse arguments to dictiodevicnary
    return vars(parser.parse_args(args))


def load_default_config(task):
    """
    Load default parameter configuration from file.
    Args:
        tasks: String with the task name

    Returns:
        Dictionary of default parameters for the given task
    """

    if task == "doq":
        default_config = "etc/doq.json"
    else:
        raise ValueError("Task \"{}\" not defined.".format(task))

    with open(default_config) as config_json_file:
        cfg = json.load(config_json_file)
    


    return cfg



def count_object_in_notes(data_doq, object_name):
    '''
    For a given ACLED dataframe (alerady preprocessed) and a given object_name (string), collect all the quantities related to that object in the notes

    data_doq: (Preprocessed) ACLED dataframe; not relevant as long as the notes are included
    object_name: string, name of the object we want to count
    n_rows: Number of rows to process (if we include all, it may take a few minutes)

    '''
    # Create a list of the entries of one specific column
    notes = data_doq.NOTES.tolist()
    # Create the count for one specific object
    
    object_count = numutils.collect_quantities_for_object(notes, object_name)
    print("Object count for:", object_name, " :", object_count)
    
    return object_count


def load_and_preprocess_ACLED(n_rows = -1):
    '''
    Load ACLED data into datafame, add columns if needed and possibly shuffle. Can also load only part of the dataset

    '''

    # Load data as dataframe,  last argument 'TRUE' to shuffle the rows
    data_doq = datautils.acled_data_to_df('../data/gender_Feb26.xlsx', 'Sheet1', '../data/gender.csv', True) 

    if (n_rows!= -1):
        if (n_rows >= len(data_doq)):
            print("N_rows is not smaller than the length of the dataframe, which is", len(data_doq))
        else:
            # Cut size of dataframe if speicified
            data_doq = data_doq[:n_rows]

    # Additional columns to store extracted quantities 
    additional_columns = [('QUANTITY', 0), ('SIZE_ANNOTATED', -1)]
    data_doq["FATALITIES"] = pd.to_numeric(data_doq["FATALITIES"], errors = 'coerce')
    data_doq = datautils.extend_dataframe_by_columns(data_doq, additional_columns)

    return data_doq

def collect_and_visualize_doq(df_doq, count_item_list, context, log = True):
    ''' For a given count_item, collect the doq and visualize as violin plot

    '''



    # Create list of counts by iterating over df_doq for relevant count_item
    quant_lists = []
    for count_item in count_item_list:
        quant_list = []
        for index, row in df_doq.iterrows():
            # if row matches context
            if context.context_fulfilled(row): 
                quant_list = quant_list + [int(row[count_item])]    # add entry to list

        # From list, create violin plot
        print("Quant_list for context: ", context, " :", quant_list)
        sns.set_theme(style="whitegrid")
        
        if (log == True):
            quant_list = utils.my_log10(quant_list)
        
        quant_lists.append(quant_list)

    #log_female_count = [log_women_count, log_girl_count]
    ax = sns.violinplot(x= quant_list, cut = 0)
    ax.set_title(f"Distribution for {count_item} conditioned on  {context}")
    plt.show()

    # Create new dataframe

    

    
   # plt.boxplot(quant_list)
  #  plt.show()


def doq(cfg):
    logging.info("Start doq experiment with parametrization:\n{}".format(json.dumps(cfg, indent=4)))

    data_doq = load_and_preprocess_ACLED(cfg['n_rows'])

    # Initialize methods
    # For each method, initialize statistics
    absolute_error = 0

    fatality_count = 0
    detected = 0
    detected_irrelevant = 0
    detected_window_verb = 0
    detected_window_noun = 0
    detected_rule = 0
    squared_error = 0

    # Statistics
    n_irrelevant = 0
    tp = 0
    fp = 0
    for index, row in data_doq.iterrows():
        # cut out [size_count]
        datautils.find_annotated_size_acled(row)
        if (index == 0): continue
        num = 0
        # go over all rules that are activated
        if (cfg['nsubj_pass']==True):
            data_doq.at[index, "nsubj_pass"] = numutils.count_nsubjpass_for_action(row["NOTES"], cfg['action_set'], cfg['object_set'])
        if (cfg['dobj_verb']==True):
            data_doq.at[index, "dobj_verb"] = numutils.count_dobj_for_action(row["NOTES"], cfg['action_set'], cfg['object_set'])
        if (cfg['verb_window']== True):
            data_doq.at[index, "verb_window"] = numutils.extract_action_count_in_window(row["NOTES"], cfg['window_size'], cfg['action_set'])
        if (cfg['noun_window'] == True):
            data_doq.at[index, "noun_window"] = numutils.extract_noun_count_in_window(row["NOTES"], cfg['window_size'], cfg['noun_set'])

        if (cfg['skip_irrelevant'] and not manual_rules.main_action_is_interesting(row["NOTES"], cfg['action_set'])):
            data_doq.at[index, "irrelevant"] = True

        # true_num = int(row["FATALITIES"])
        # fatality_count = fatality_count + true_num
        # if (num > true_num): 
        #     print("num:", num, "true_num:", row["FATALITIES"])
        #     print(row["EVENT_TYPE"], row["NOTES"])
            
        # squared_error = squared_error + (num - true_num) * (num - true_num)
        # # Update statistics
        # if (num <= true_num):
        #     tp = tp +  num
        # else:
        #     fp = fp + (num - true_num)
    
    if (cfg['plot_doq']):
        context = doq_models.context("FATALITIES", "YEAR", ["2018", "2019", "2020"], "COUNTRY", ["Afghanistan"])
        context.plot_doq(data_doq)
    

   # ax = sns.violinplot(x = data_doq["FATALITIES"])
   # ax = sns.violinplot(x = data_doq["nsubj_pass"])
    plt.scatter(data_doq["FATALITIES"], data_doq["nsubj_pass"], alpha = 0.3, label='nsubj_pass')
    plt.scatter(data_doq["FATALITIES"], data_doq["dobj_verb"], alpha = 0.3,  label= 'dobj_verb')
    plt.scatter(data_doq["FATALITIES"], data_doq["noun_window"],alpha = 0.3,  label = 'noun_window')
    plt.scatter(data_doq["FATALITIES"], data_doq["verb_window"],alpha = 0.3,  label = 'verb_window')
   # plt.plot(data_doq["nsubj_pass"])
    plt.legend()
    plt.xlim((0,20))
    plt.ylim((0,20))
    plt.savefig("validate_against_fatalities.png")
    

    data_doq.to_csv("data_doq.csv")

    


    # print("prozent falscher zahlen:", fp/detected)
    # print("RMSE:", np.sqrt(squared_error/len(data_doq)))
    # print("dectected: ", detected)
    # print("fatalities:", fatality_count)
    # print("detected rule", detected_rule)
    # if (fatality_count > 0):
    #     print("richtig erkannt:", tp/fatality_count)
    # print("number of irrelevant notes: ", n_irrelevant)
    # print("detected irrelevant:", detected_irrelevant)


    # print f1 scores
    if (cfg['nsubj_pass']==True):
        print("f1 measure for nsubjs_pass:", utils.calculate_f1_for_method(data_doq, "nsubj_pass", "FATALITIES"))
    if (cfg['dobj_verb']==True):
        print("f1 measure for dobj_verb:", utils.calculate_f1_for_method(data_doq, "dobj_verb", "FATALITIES"))
    if (cfg['verb_window']== True):
        print("f1 measure for verb_window:", utils.calculate_f1_for_method(data_doq, "verb_window", "FATALITIES"))
    if (cfg['noun_window'] == True):
        print("f1 measure for noun_window:", utils.calculate_f1_for_method(data_doq, "noun_window", "FATALITIES"))


if __name__ == "__main__":


        # Parse the shell arguments as input configuration
    user_config = parse_shell_args(sys.argv[1:])

    # Load default parameter configuration from file
    cfg = load_default_config("doq")

    # Overwrite default parameters with user configuration where specified
    cfg.update(user_config)
    doq(cfg)

    print('Done!')