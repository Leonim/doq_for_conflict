#  Load female acled data


import csv
import doq, datautils

n_rows = 100

#data = datautils.acled_data_to_df('../data/gender_Feb26.xlsx', 'Sheet1','../data/gender.csv', shuffle_rows = True)

data = doq.load_and_preprocess_ACLED(2*n_rows)

data = data[data.FATALITIES == 0]
data = data[:n_rows]

additional_columns = [('ANNOTATED_NUMBER', -1), ('COMMENT', 'no comment')]
data = datautils.extend_dataframe_by_columns(data, additional_columns)

# save
data.to_csv('../data/annotate_subset.csv')

# interface: print row, enter annotation
for index, row in data.iterrows():
    print("Note:", row['NOTES'])
    number = input("What is the number? ")
    
    try:
        data.at[index, 'ANNOTATED_NUMBER'] = number
    except: 
        pass

    comment = input("Any comment? ")
    
    if (comment != '0'):
        try: 
            data.at[index, 'COMMENT'] = comment
        except:
            pass


data.to_csv('../data/annotate_subset.csv')