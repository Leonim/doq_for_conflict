import torch
import torchtext.legacy.data as data

SEED = 1234
import pandas as pd
import numpy as np
torch.manual_seed(SEED)
torch.cuda.manual_seed(SEED)
torch.backends.cudnn.deterministic = True

import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
import torchtext

import nltk

import random
from sklearn.metrics import classification_report

import pyprind

# Imports


# ARGUMENTS
'''Training types: totally blinded (no BERT embeddings), partial BERT/Glove embeddings, only words with BERT/Glove embeddings'''
import  doq

# DATA LOADING ---------------------------------------
# Load the data
data_doq = doq.load_and_preprocess_ACLED(100)
print(len(data_doq))

# Create new dataframe with feature (note)
data_doq_fat = data_doq[data_doq['FATALITIES'] != 0]

df = data_doq_fat[['NOTES', 'FATALITIES']].copy()

# split into train, test and validation data
def split_train_test_valid(data, ratio_train, ratio_test):

    n_data = len(data)
    n_train = int(n_data * ratio_train)
    n_test = int(n_data * ratio_test)
    data_train = data.iloc[:n_train]
    data_test = data.iloc[n_train:(n_train + n_test)]
    data_valid = data.iloc[(n_train + n_test) :]

    return data_train, data_test, data_valid

data_train, data_test, data_valid = split_train_test_valid(df, 0.5, 0.4)

data_train.to_csv('doq_extraction/torchtext_data/train.csv', index =False)
data_test.to_csv('doq_extraction/torchtext_data/test.csv', index =False)
data_valid.to_csv('doq_extraction/torchtext_data/valid.csv', index =False)
del data_train, data_test, data_valid, df, data_doq, data_doq_fat


import spacy
spacy_en = spacy.load('en_core_web_sm')

is_cuda = torch.cuda.is_available()
print("Cuda Status on system is {}".format(is_cuda))

# sample tokenizer which you can use
def tokenizer(text):
    return [tok for tok in nltk.word_tokenize(text)]

# tokenizer = "spacy" uses spacy's tokenizer
TEXT = data.Field(sequential=True, tokenize="spacy")
LABEL = data.Field(dtype=torch.long, sequential=False) # changed from data.LabelField
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
# loading train, test and validation data 
train_data, valid_data, test_data = data.TabularDataset.splits(
    path="doq_extraction/torchtext_data/", train="train.csv", 
    validation="valid.csv", test="test.csv",format="csv", skip_header=True, 
    fields=[('Text', TEXT), ('Label', LABEL)]
)

print(f'Number of training examples: {len(train_data)}')
print(f'Number of valid examples: {len(valid_data)}')
print(f'Number of testing examples: {len(test_data)}')

TEXT.build_vocab(train_data, vectors=torchtext.vocab.Vectors("embeddings/glove.840B.300d.txt"), 
                 max_size=20000, min_freq=10)
LABEL.build_vocab(train_data)
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

print(f"Unique tokens in TEXT vocabulary: {len(TEXT.vocab)}")
print(f"Unique tokens in LABEL vocabulary: {len(LABEL.vocab)}")

BATCH_SIZE = 2

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

# keep in mind the sort_key option 
train_iterator, valid_iterator, test_iterator = data.BucketIterator.splits(
    (train_data, valid_data, test_data), sort_key=lambda x: len(x.Text),
    batch_size=BATCH_SIZE,
    device=device)

class RNN(nn.Module):
    def __init__(self, input_dim, embedding_dim, hidden_dim, output_dim):
        super().__init__()
        
        self.embedding = nn.Embedding(input_dim, embedding_dim)
        self.rnn = nn.RNN(embedding_dim, hidden_dim)
        self.fc = nn.Linear(hidden_dim, output_dim)
        
    def forward(self, x):

        #x = [sent len, batch size]
        
        embedded = self.embedding(x)
        
        #embedded = [sent len, batch size, emb dim]
        
        output, hidden = self.rnn(embedded)
        
        #output = [sent len, batch size, hid dim]
        #hidden = [1, batch size, hid dim]
        
        assert torch.equal(output[-1,:,:], hidden.squeeze(0))
        
        out = self.fc(hidden)
        return out

INPUT_DIM = len(TEXT.vocab)
EMBEDDING_DIM = 300
HIDDEN_DIM = 374
OUTPUT_DIM = 100

model = RNN(INPUT_DIM, EMBEDDING_DIM, HIDDEN_DIM, OUTPUT_DIM)

pretrained_embeddings = TEXT.vocab.vectors

model.embedding.weight.data = pretrained_embeddings
#class_weights = torch.tensor([1.0, 15.0])
optimizer = optim.SGD(model.parameters(), lr=1e-2)
criterion = nn.CrossEntropyLoss()
model = model.to(device)
criterion = criterion.to(device)

def get_label(prediction):
    ind = -100
    try:
        preds, ind= torch.max(F.softmax(prediction.squeeze(0), dim=-1), 1)
    except:
        pass
    return ind


def binary_accuracy(preds, y):
    """
    Returns accuracy per batch, i.e. if you get 8/10 right, this returns 0.8, NOT 8
    """

    preds, ind= torch.max(F.softmax(preds, dim=-1), 1)
    correct = (ind == y).float()
    acc = correct.sum()/float(len(correct))
    return acc

def train(model, iterator, optimizer, criterion):
    
    epoch_loss = 0
    epoch_acc = 0
    
    model.train()
    bar = pyprind.ProgBar(len(iterator), bar_char='█')
    for batch in iterator:
        
        optimizer.zero_grad()
                
        predictions = model(batch.Text).squeeze(0)
#         print(predictions.shape, batch.Label.shape, model(batch.Text).shape)
        loss = criterion(predictions, batch.Label)
#         print(loss.shape)
        acc = binary_accuracy(predictions, batch.Label)
        
        loss.backward()
        
        optimizer.step()
        
        epoch_loss += loss.item()
        epoch_acc += acc.item()
        bar.update()
    return epoch_loss / len(iterator), epoch_acc / len(iterator)

def evaluate(model, iterator, criterion):
    
    epoch_loss = 0
    epoch_acc = 0
    
    model.eval()
    
    with torch.no_grad():
        bar = pyprind.ProgBar(len(iterator), bar_char='█')
        for batch in iterator:

            predictions = F.softmax(model(batch.Text).squeeze(0))

            print(get_label(predictions))

            loss = criterion(predictions, batch.Label)
            print()
            
            acc = binary_accuracy(predictions, batch.Label)

            epoch_loss += loss.item()
            epoch_acc += acc.item()
            bar.update()
    return epoch_loss / len(iterator), epoch_acc / len(iterator)

N_EPOCHS = 20

for epoch in range(N_EPOCHS):

    train_loss, train_acc = train(model, train_iterator, optimizer, criterion)
    valid_loss, valid_acc = evaluate(model, valid_iterator, criterion)
    
    print(f'| Epoch: {epoch+1:02} | Train Loss: {train_loss:.3f} | Train Acc: {train_acc*100:.2f}% | Val. Loss: {valid_loss:.3f} | Val. Acc: {valid_acc*100:.2f}% |')

test_loss, test_acc = evaluate(model, test_iterator, criterion)

print(f'| Test Loss: {test_loss:.3f} | Test Acc: {test_acc*100:.2f}% |')

def predict_sentiment(sentence):
    tokenized = [tok for tok in sentence.split()]
    indexed = [TEXT.vocab.stoi[t] for t in tokenized]
    tensor = torch.LongTensor(indexed).to(device)
    
    tensor = tensor.unsqueeze(1)
#     print(tensor.shape)
    prediction = model(tensor)
    preds, ind= torch.max(prediction.squeeze(0), 1)
#     print(preds)
    return preds, ind



predict_sentiment('Four women were shot.')[1].item()

import pandas as pd

for batch in test_iterator:
    train = pd.DataFrame(batch.dataset)
    for row in range(len(train)):
        print(train.loc[row].item().Text)
        break
    break
