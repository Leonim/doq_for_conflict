import torch
from doq_utils import utils

import torch.nn as nn


def binary_accuracy(preds, y):
    """
    Returns accuracy per batch, i.e. if you get 8/10 right, this returns 0.8, NOT 8
    """

    

    #round predictions to the closest integer
    pred_indices = torch.max(preds, dim = 1)[1]

    correct = (pred_indices == y).float() #convert into float for division 
    acc = correct.sum() / len(correct)
    return acc

def evaluate(model, embedding, iterator, textfield, labelfield, n_samples = 0):
    softmax = torch.nn.Softmax(dim=1)
    epoch_acc = 0
    model.eval()
    
    with torch.no_grad():
        for batch in iterator:
            print('batch len: ', len(batch))
            if (embedding == 'bert'):
                predictions = softmax(model(batch.text))
            else:
                text, text_lengths = batch.text
                text_dep = batch.dep
                predictions = softmax(model(text, text_lengths, text_dep))
            acc = binary_accuracy(predictions, batch.label)
            epoch_acc += acc.item()

    labels = torch.max(predictions, dim = 1)[1]
    for i in range(min(n_samples, len(labels))):
        note = utils.reverse_vocab(text[:,i].tolist(), textfield)
        print(note)
        print('predicted label: ', labelfield.vocab.itos[labels[i].item()],' true label: ', labelfield.vocab.itos[int(batch.label[i].item())])
        
    return epoch_acc / len(iterator)

# training function 
def train(model, embedding, iterator, optimizer, criterion_name, confusion_matrix):
    softmax = torch.nn.Softmax(dim=1) # TODO: Check whether we really need the softmax
    epoch_loss = 0
    epoch_acc = 0
    
    model.train()
 
    for batch in iterator:
        
        
        optimizer.zero_grad()
        if (embedding == 'bert'):
            predictions = softmax(model(batch.text))
        else:
            text, text_lengths = batch.text
            text_dep = batch.dep
            predictions = softmax(model(text, text_lengths, text_dep))

        batch.label = batch.label.long()


        if (criterion_name=='cross_entropy'):
            criterion = nn.CrossEntropyLoss()
            loss = criterion(predictions, batch.label)
        elif(criterion_name == 'my_mse'):
            loss = utils.my_mse(predictions, batch.label, confusion_matrix)
        acc = binary_accuracy(predictions, batch.label)

        loss.backward()
        optimizer.step()
        
        epoch_loss += loss.item()
        epoch_acc += acc.item()
        

    return epoch_loss / len(iterator), epoch_acc / len(iterator)