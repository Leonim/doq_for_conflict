from string import punctuation
from os import listdir
from nltk.corpus import stopwords
import numpy as np
import torch
import pandas as pd
import spacy


from doq_utils import config




''' Functions'''


def parse_string(text, language_pipeline = "en_core_web_sm"):
	'''Creates a parser and parses a string
	Args:
	    text: string file to be parsed
		langauge_pipeline: Language pipeline to be used. "sm" stands for small and is enough for most tasks. Components: tok2vec, tagger, parser, senter, ner, attribute_ruler, lemmatizer.
	
	Returns: 
		doc: processed doc of the input text. Split into individual words and annotated. No information is lost, all whitespaces and characters maintained.
		'''

	nlp = spacy.load(language_pipeline)
	doc = nlp(text)
	return doc


def visualize_dependency_parse_of_slice(doc_slice, style = 'dep'):
	'''Visualizes a slice of the parsed text'''
	svg = spacy.displacy.serve(doc_slice, style = 'dep')



# load doc into memory
def load_doc(filename):
	# open the file as read only
	file = open(filename, 'r')
	# read all text
	text = file.read()
	# close the file
	file.close()
	return text

# turn a doc into clean tokens
def clean_doc(doc):
	# split into tokens by white space
	tokens = doc.split()
	# remove punctuation from each token
	#table = str.maketrans('', '', punctuation)
	#tokens = [w.translate(table) for w in tokens]
	# remove remaining tokens that are not alphabetic
	#tokens = [word for word in tokens if word.isalpha()]
	# filter out stop words
	#stop_words = set(stopwords.words('english'))
	#tokens = [w for w in tokens if not w in stop_words]
	# filter out short tokens
	tokens = [word for word in tokens if len(word) > 1]
	return tokens


# load all docs tokenized in a directory
def tokenize_docs(directory):
	lines = list()
	# walk through all files in the folder
	for filename in listdir(directory):
		# skip files that do not have the right extension
		if not filename.endswith(".txt"):
			continue
		# create the full path of the file to open
		path = directory + '/' + filename
        #doc = load_doc(path)
		line = clean_doc(load_doc(path))
		# add to list
		lines.append(line)
	return lines


def f_measure(tp, fp, fn):
    precision = tp/(tp + fp)
    recall = tp/(fn + tp)
    return 2/(1/precision + 1/recall)

def my_log10(list):
	'''
	Computes log 10 on a list but is safe to apply because it returns 0 for all values < 1, too.
	'''
	res = []
	for x in list:
		if (x == 'NaN'): continue
		if (x < 1) : 
			res.append(0)
		else: 
			res.append(np.log10(x))

	return res


def calculate_statistics_for_method(df, method, true_value):
    tp = 0
    fn = 0
    fp = 0
    for index, row in df.iterrows():
        if (row[method] <= row[true_value]): 
            tp = tp + row[method]
            fn = fn + row[true_value] - row[method]
        else:
            tp = tp + row[true_value]
            fp = fp -row[true_value] + row[method]
    
    return tp, fp, fn

def calculate_f1_for_method(df, method, true_value):

	tp, fp, fn = calculate_statistics_for_method(df, method, true_value)
	f1 = f_measure(tp, fp, fn)
	return f1

def normalise_text (text):
    text = text.str.lower() # lowercase
    text = text.str.replace(r"\#","") # replaces hashtags
    text = text.str.replace(r"http\S+","URL")  # remove URL addresses
    text = text.str.replace(r"@","")
    text = text.str.replace(r"[^A-Za-z0-9()!?\'\`\"]", " ")
    text = text.str.replace("\s{2,}", " ")
    return text

def reverse_vocab(textlist, textfield):
    wordlist = [textfield.vocab.itos[x] for x in textlist]
    note = ' '.join(wordlist)
    return note


def label_confusion_matrix(labelfield, n_dim):
	'''
	Create the confusion matrix for a particular target vocabulary. Row i, Col j: Squared error when true label is i (in vocab)and value j is predicted
	
	'''
	labellist = labelfield.vocab.itos
	while (len(labellist) < n_dim):
		labellist.append(0)
	confusion_matrix = []
	for label in labellist:
		error= [(x -int(label))*(x-int(label)) for x in labellist]
		confusion_matrix.append(error)
	
	return confusion_matrix

def my_mse(predictions, batchlabel, confusion_matrix):
	'''
	My implementation of a mean squared error, taking the real distance between labels into account
	
	'''
	label_errors = []
	for label in batchlabel:
		label_errors.append(confusion_matrix[int(label.item())])

	label_errors = torch.tensor(label_errors)

	pred_errors = torch.mean(predictions * label_errors)
	pred_error= torch.mean(pred_errors)
	return pred_error