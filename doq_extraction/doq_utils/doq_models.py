import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
from doq_utils import utils

class Extractor:

    # Class attributes

    def __init__(self, name,  word_list, rule):
        Extractor.name = name
        Extractor.word_list = word_list
        Extractor.rule = rule

    def __str__(self):
        return f"{self.name} is using the word list {self.word_list}"
    
    # Extract quantifiers from note

    # Calculate statistics for method -- precision and recall
    def f_measure(self, tp, fp, fn):
        precision = tp/(tp + fp)
        recall = tp/(fn + tp)
        return 2/(1/precision + 1/recall)


    # Process dataframe

    

    # 

class context:

    def __init__(self, count_item, context_1_cat = "ALL", context_1_list = [], context_2_cat = "ALL", context_2_list = []):

        self.count_item = count_item
        self.context_1_cat = context_1_cat
        self.context_1_list = context_1_list
        self.context_2_cat = context_2_cat
        self.context_2_list = context_2_list # only can handle a single additional context
    
    def __str__(self):

        info = self.count_item + "_"

        info = info  + self.context_1_cat + "_"
        for context_1 in self.context_1_list:
            info = info + context_1 + "_"
        
        info = info + self.context_2_cat + "_"
        for context_2 in self.context_2_list:
            info = info + context_2 
        print(info)
        return info

    
    def plot_doq(self, data_doq):
        '''
    condition_1: tuple (column_name, values)

        '''
        plot_doq = data_doq
        if (self.context_1_cat != "ALL" ):
            plot_doq = data_doq[data_doq[self.context_1_cat].isin(self.context_1_list)]
        if (self.context_2_cat !=  "ALL"):
            plot_doq = plot_doq[plot_doq[self.context_2_cat].isin(self.context_2_list)]
        
        # Create plot
        if (self.context_1_cat != "ALL"):
            ax = sns.violinplot(x=self.context_1_cat, y=self.count_item, data=plot_doq, cut = 0)
        else:
            ax = sns.violinplot(x=plot_doq[self.count_item], cut = 0)
        plt.savefig("doq_plots/" + str(self) + ".png")
        plt.close()




class context_old:
    def __init__(self, year = 0, gender = "BOTH", fatalities = -1, event_type = "ALL", country = "ALL", actor1 = "ALL", actor2 = "ALL"):
        self.year = year
        self.gender = gender
        self.fatalities = fatalities
        self.event_type = event_type
        self.country = country
        self.actor1 = actor1
        self.actor2 = actor2
    
    def __str__(self):
        info = " "
        if (self.year != 0): info = info + "Year: " + str(self.year) + ", "
        if (self.gender != "BOTH"): info = info + "Gender: " + self.gender + ", "
        if (self.fatalities != -1): info = info + "Fatalities <= " + str(self.fatalities) + ", "
        if (self.event_type != "ALL"): info = info + "Event type: " + self.event_type + ", "
        if (self.country != "ALL"): info = info + "Country: " + self.country + ", "
        if (self.actor1 != "ALL"): info = info + "Actor1: " + self.actor1 + ", "
        if (self.actor2 != "ALL"): info = info + "Actor2: " + self.actor2 + ", "

        return info
    
    def context_fulfilled(self, df_row):

        if (self.year != 0 and df_row["YEAR"] != self.year):
            return False
        
        if (self.gender != "BOTH" and df_row["GENDER"] != self.gender):
            return False
        
        if (self.fatalities != -1 and df_row["FATALITIES"] > self.fatalities): 
            # all events with less fatalities than the specified number are counted in context
            return False
        
        if (self.event_type != "ALL" and df_row["EVENT_TYE"] != self.event_type):
            return False
        
        if (self.country != "ALL" and df_row["COUNTRY"] != self.country):
            return False
        
        if (self.actor1 != "ALL" and df_row["ACTOR1"] != self.actor1):
            return False
        
        if (self.actor2 != "ALL" and df_row["ACTOR2"] != self.actor2):
            return False
        
        # Context is matched - nothing exlcuded
        return True

  