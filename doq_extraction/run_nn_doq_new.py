import numpy as np
import spacy
import faulthandler


import argparse
import torchtext.data as data
import torch

import pandas as pd
import datetime


import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
import time
import revtok


import torch.nn as nn
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
import sys
import json
import logging
import seaborn as sns
import pickle
import torchtext.legacy.data as data


from doq_utils import utils, datautils, numutils, manual_rules, doq_models, preprocessing_nn, doq_nn_models, doq_nn_train, config
import doq

# PREPROCESSING



def parse_shell_args(args):

    parser = argparse.ArgumentParser(description='')

    # LOGGIN ARGS
    parser.add_argument("--log_dir", type=str, default="",
                        help="Subdirectory within ./log/ where to store logs.")

    parser.add_argument('--task', default='acled', help='Dataset on which to run the task')
    parser.add_argument('--n_rows',default=argparse.SUPPRESS, help='Specify if you want to cut the size of the dataset', type=int)


    # PREPROCESSING ARGS
    parser.add_argument('--relevant_fatality_numbers', nargs = '+', default=argparse.SUPPRESS,help='Specify if you want to train only with a subset of fatility numbers', required=False)
    parser.add_argument('--balance_dataset_evenly', type=bool, help='Balance dataset evenly if you want the same number of event notes for each number of fatalities. Currently determined by the size of the smallest training set per fat. number. May lead to a very small dataset.', default = False)
    parser.add_argument('--remove_dates', type=bool, default=argparse.SUPPRESS,help='All dates in notes are deleted if true',required = False)
    parser.add_argument('--remove_size_count', type=bool, default=argparse.SUPPRESS,help='If notes contain a size count, it is removed',required = False)
    parser.add_argument('--save_data_doq', type=bool, default=argparse.SUPPRESS, help='True if we want to save a reprocessed data_doq', required=False)

    # DATA LOADING ARGS
    parser.add_argument('--token_info', nargs = '+', default=argparse.SUPPRESS, help='Choose of list of info types that we want to use for training: text, dep, pos, blacked text, ner.',required = False)
    parser.add_argument('--embedding', type=str, default=argparse.SUPPRESS, help='Emebdding type to use, choose between glove and bert',required = False)
    parser.add_argument('--context', nargs = '+', default=argparse.SUPPRESS, help='Which other ACLED columns to use for training',required = False)
    parser.add_argument('--create_vocab', type=bool, default=argparse.SUPPRESS, help='True if we want the BERT/Glove vocab to be created', required=False)

    # MODEL ARGS
    parser.add_argument('--model', type=str, default=argparse.SUPPRESS, help='Type of the neural network',required = False)
    parser.add_argument('--output_representation', type=str,default=argparse.SUPPRESS, help='How to represent the extracted/predicted number: Multiclass classification on linear axis/log axis; with embedding--> vocab, sth else?',required = False)
    parser.add_argument('--learning_rate', type = float, default=argparse.SUPPRESS, help='Learning rate of the neural network',required = False )
    parser.add_argument('--optimizer', type=str, default=argparse.SUPPRESS, help='Choose from adam, adagrad, sgd.', required = False )
    parser.add_argument('--context_model_dim', nargs='+', default=argparse.SUPPRESS, help='layer dimensions of MLP to classify context',required = False)

    # TRAINING ARGS
    parser.add_argument('--criterion', type=str, default=argparse.SUPPRESS, help='Criterion to calculate the loss')
    parser.add_argument('--epochs', type=int, default=argparse.SUPPRESS, help='Number of training epochs')
    parser.add_argument('--loss', type=str, default=argparse.SUPPRESS, help='Loss type. Think of loss types that take distance into account')




    return vars(parser.parse_args(args))

def load_default_config(task):
    """
    Load default parameter configuration from file.
    Args:
        tasks: String with the task name

    Returns:
        Dictionary of default parameters for the given task
    """

    if task == "acled":
        default_config = "doq_utils/etc/acled_nn.json"
    elif task == "emm":
        default_config = "doq_utils/etc/emm_nn.json"
    else:
        raise ValueError("Task \"{}\" not defined.".format(task))

    with open(default_config) as config_json_file:
        cfg = json.load(config_json_file)

    return cfg

def load_and_preprocess_task(cfg):
    if (cfg['task'] == 'acled'):
        data_doq = datautils.load_and_preprocess_ACLED(cfg['n_rows'], cfg['remove_size_count'], cfg['context'], cfg['relevant_fatality_numbers'], cfg['balance_dataset_evenly'])
    elif (cfg['task']== 'emm'):
        data_doq = datautils.load_and_preprocess_EMM(cfg['n_rows'], cfg['context_emm'], cfg['target_emm'], cfg['relevant_fatality_numbers'], cfg['balance_dataset_evenly'])

    preprocessing_nn.extend_data_parse(data_doq) # Add dep, ner and pos parsese to datadoq

    return data_doq
    


def doq_nn(cfg, Path):
    logging.info("Start doq experiment with parametrization:\n{}".format(json.dumps(cfg, indent=4)))


    # Load data
    if(cfg['load_data'] == True):
        Path = '../data/data_doq_preprocessed_' + cfg['task'] + '.pkl'
        data_doq = pd.read_pickle(Path)
    else:
        data_doq = load_and_preprocess_task(cfg)
        data_doq.to_pickle('../data/data_doq_preprocessed_'+ cfg['task'] + '.pkl')


    # TODO: Add blackened text to data_doq

    # Create fields
    TEXT, LABEL, DEP, POS, ENT, fields, tokenizer = preprocessing_nn.create_fields(cfg['embedding'])

    # split acled data into train, validation and test data
    val_size= 0.3
    test_size = 0.2
    train_ds, val_ds, test_ds = preprocessing_nn.create_train_val_test_split(data_doq, fields, val_size, test_size)

    # Create vocabularies
    preprocessing_nn.build_vocabs(TEXT, LABEL, DEP, POS, ENT, cfg['embedding'], train_ds, cfg['task'], cfg['relevant_fatality_numbers'],  cfg['create_vocab'])

    # MODEL

    BATCH_SIZE = 128

    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    # Intitialize train and validation iterators

    train_iterator, valid_iterator, test_iterator = data.BucketIterator.splits(
        (train_ds, val_ds, test_ds), 
        batch_size = BATCH_SIZE,
        sort = False,
        sort_within_batch = True,
        sort_key=lambda x: len(x.text),
        device = device)
    
 
    
    
    # Create model that takes in several embeddings
    OUTPUT_DIM = len(cfg['relevant_fatality_numbers']) + 1
    model = doq_nn_models.initialize_model(cfg['embedding'], cfg['model'], TEXT, DEP, OUTPUT_DIM)
    model.to(device) #CNN to GPU

    # Loss and optimizer
    # initialize confusion matrix for loss:
    confusion_matrix = utils.label_confusion_matrix(LABEL, OUTPUT_DIM)

    criterion = cfg['criterion']

    num_epochs = cfg['epochs']
    learning_rate = cfg['learning_rate']
    optimizer = torch.optim.Adam(model.parameters())


    # TRAINING

    t = time.time()
    loss=[]
    acc=[]
    val_acc=[]
    best_val_acc = float('inf')
    best_model_path = Path +'/best_model.pt'

    for epoch in range(num_epochs):
        
        train_loss, train_acc = doq_nn_train.train(model, cfg['embedding'], train_iterator, optimizer, criterion, confusion_matrix)
       # try:
        valid_acc = doq_nn_train.evaluate(model, cfg['embedding'], valid_iterator, TEXT, LABEL) #sometimes the function throws a key error, seems to be a bug
       # except:
           # valid_acc = 0


        
        logging.info(f'\tTrain Loss: {train_loss:.3f} | Train Acc: {train_acc*100:.2f}%')
        logging.info(f'\t Val. Acc: {valid_acc*100:.2f}%')
        
        loss.append(train_loss)
        acc.append(train_acc)
        val_acc.append(valid_acc)

        if valid_acc < best_val_acc:
            best_val_acc = valid_acc
            torch.save(model.state_dict(), best_model_path)
        
    logging.info(f'time:{time.time()-t:.3f}')

    model.load_state_dict(torch.load(best_model_path))
    test_acc = doq_nn_train.evaluate(model, cfg['embedding'], test_iterator, TEXT, LABEL) 
    # logging.info(f' Test Acc: {test_acc*100:.2f}%')




    # VISUALIZE
    plt.figure()
    plt.xlabel("runs")
    plt.ylabel("normalised measure of loss/accuracy")
    x_len=list(range(len(acc)))
    plt.axis([0, max(x_len), 0, 1])
    plt.title('result of LSTM')
    loss=np.asarray(loss)/max(loss)
    plt.plot(x_len, loss, 'r.',label="loss")
    plt.plot(x_len, acc, 'b.', label="accuracy")
    plt.plot(x_len, val_acc, 'g.', label="val_accuracy")
    plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.2)
    plt.savefig(Path + '/training.png', bbox_inches = 'tight')








if __name__ == "__main__":
    faulthandler.enable()

    # Parse the shell arguments as input configuration
    user_config = parse_shell_args(sys.argv[1:])

    # Load default parameter configuration from file
    cfg = load_default_config(user_config['task'])

    # Overwrite default parameters with user configuration where specified
    cfg.update(user_config)

    # Setup global logger and logging directory
    Path = datetime.datetime.now().strftime("%Y%m%d_%H%M_")+ cfg["embedding"]+"_"+"" + cfg["task"]
    config.setup_logging(Path, dir=cfg['log_dir'])

    doq_nn(cfg, 'log/'+ Path)

    print('Done!')
